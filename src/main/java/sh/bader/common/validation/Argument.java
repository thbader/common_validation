package sh.bader.common.validation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Argument {
    private final String key;

    private final LinkedList<Object> values = new LinkedList<>();

    public Argument(final String key, final List<?> values) {
        this.key = key;
        this.values.addAll(values);
    }

    public String getKey() {
        return this.key;
    }

    public List<Object> getValues() {
        return Collections.unmodifiableList(this.values);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("key", this.key)
            .append("values", this.values)
            .toString();
    }
}
