package sh.bader.common.validation;

import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public final class Result {

    public enum Type {
        SUCCESS(true),
        ERROR(false),
        IGNORED(true);

        boolean valid;

        Type(final boolean valid) {
            this.valid = valid;
        }

        public boolean isValid() {
            return this.valid;
        }
    }

    private static final Result SUCCESS = new Result(Type.SUCCESS);

    private static final Result IGNORED = new Result(Type.IGNORED);

    private final Type type;

    private final Error error;

    private Result(final Type type) {
        this(type, null);
    }

    private Result(final Type type, final Error error) {
        this.type = type;
        this.error = error;
    }

    public static Result success() {
        return Result.SUCCESS;
    }

    public static Result ignored() {
        return Result.IGNORED;
    }

    public static Result error(final Error error) {
        return new Result(Type.ERROR, error);
    }

    public Type getType() {
        return this.type;
    }

    public Optional<Error> getError() {
        return Optional.ofNullable(this.error);
    }

    public boolean isValid() {
        return this.type.isValid();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("type", this.type)
            .append("error", this.error)
            .toString();
    }
}
