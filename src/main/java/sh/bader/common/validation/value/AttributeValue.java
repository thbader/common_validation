package sh.bader.common.validation.value;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Errors;
import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.conditional.ConditionalValidators;

public class AttributeValue<T> {
    private final Attribute<T> attribute;

    private final boolean isNew;

    private final Errors errors;

    public AttributeValue(final Attribute<T> attribute, final boolean isNew, final Errors errors) {
        this.attribute = attribute;
        this.isNew = isNew;
        this.errors = errors;
    }

    public Attribute<T> getAttribute() {
        return attribute;
    }

    public String getPath() {
        return this.getAttribute().getPath();
    }

    public Optional<T> getValue() {
        return this.getAttribute().getValue();
    }

    public boolean isNew() {
        return isNew;
    }

    protected Errors getErrors() {
        return errors;
    }

    public boolean isValid() {
        return !this.errors.hasAttributeErrors(this.getAttribute().getName());
    }

    @SafeVarargs
    public final void validate(final ConditionalValidators<T>... conditionalValidators) {
        final List<AttributeValidator<T>> validators = Stream.of(conditionalValidators)
            .filter(Objects::nonNull)
            .map(validator -> validator.getValidators(this))
            .flatMap(Collection::stream)
            .collect(Collectors.toUnmodifiableList());

        this.validate(validators);
    }

    @SafeVarargs
    public final void validate(final AttributeValidator<T>... validators) {
        this.validate(Stream.of(validators)
            .filter(Objects::nonNull)
            .collect(Collectors.toUnmodifiableList())
        );
    }

    private void validate(final List<AttributeValidator<T>> validators) {
        for (final AttributeValidator<T> validator : validators) {
            // ungültige Validatoren sollen ignoriert werden
            if (validator == null) {
                continue;
            }

            // brich die Validierung ab, wenn es bereits Fehler für das Attribute gibt
            if (errors.hasAttributeErrors(this.getAttribute().getPath())) {
                break;
            }

            // validiere das Attribute und speicher ggfs. Fehler
            validator.validate(this.getAttribute()).getError().ifPresent(errors::reject);
        }
    }
}
