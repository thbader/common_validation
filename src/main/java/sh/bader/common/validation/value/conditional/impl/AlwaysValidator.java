package sh.bader.common.validation.value.conditional.impl;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.conditional.AbstractConditionalValidators;

public final class AlwaysValidator<T> extends AbstractConditionalValidators<T> {
    @SafeVarargs
    public AlwaysValidator(final AttributeValidator<T>... validators) {
        super(value -> true, validators);
    }
}
