package sh.bader.common.validation.value.conditional.impl;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.AttributeValue;
import sh.bader.common.validation.value.conditional.AbstractConditionalValidators;

import static java.util.function.Predicate.not;

public final class OnUpdateValidators<T> extends AbstractConditionalValidators<T> {
    @SafeVarargs
    public OnUpdateValidators(final AttributeValidator<T>... validators) {
        super(not(AttributeValue::isNew), validators);
    }
}
