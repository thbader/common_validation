package sh.bader.common.validation.value.conditional.impl;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.AttributeValue;
import sh.bader.common.validation.value.conditional.AbstractConditionalValidators;

public final class OnCreateValidators<T> extends AbstractConditionalValidators<T> {
    @SafeVarargs
    public OnCreateValidators(final AttributeValidator<T>... validators) {
        super(AttributeValue::isNew, validators);
    }
}
