package sh.bader.common.validation.value;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.conditional.ConditionalValidators;
import sh.bader.common.validation.value.conditional.impl.AlwaysValidator;
import sh.bader.common.validation.value.conditional.impl.OnCreateValidators;
import sh.bader.common.validation.value.conditional.impl.OnUpdateValidators;

public final class ValueValidators {
    private ValueValidators() {
    }

    @SafeVarargs
    public static <T> ConditionalValidators<T> always(final AttributeValidator<T>... validators) {
        return new AlwaysValidator<>(validators);
    }

    @SafeVarargs
    public static <T> ConditionalValidators<T> onCreate(final AttributeValidator<T>... validators) {
        return new OnCreateValidators<>(validators);
    }

    @SafeVarargs
    public static <T> ConditionalValidators<T> onUpdate(final AttributeValidator<T>... validators) {
        return new OnUpdateValidators<>(validators);
    }

}
