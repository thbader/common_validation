package sh.bader.common.validation.value.conditional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.AttributeValue;

public abstract class AbstractConditionalValidators<T> implements ConditionalValidators<T> {

    private final Predicate<AttributeValue<T>> predicate;

    private final List<AttributeValidator<T>> validators;

    @SafeVarargs
    public AbstractConditionalValidators(final Predicate<AttributeValue<T>> predicate, final AttributeValidator<T>... validators) {
        this.predicate = predicate;
        this.validators = Stream.of(validators)
            .filter(Objects::nonNull)
            .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public List<AttributeValidator<T>> getValidators(final AttributeValue<T> value) {
        if (predicate.test(value)) {
            return validators;
        }

        return Collections.emptyList();
    }
}
