package sh.bader.common.validation.value.conditional;

import java.util.List;

import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.AttributeValue;

public interface ConditionalValidators<T> {
    List<AttributeValidator<T>> getValidators(final AttributeValue<T> value);
}
