package sh.bader.common.validation.value;

import java.math.BigDecimal;
import java.util.Objects;

import org.apache.commons.lang3.compare.ComparableUtils;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Errors;

public class Value<T> extends AttributeValue<T> {
    private final Attribute<T> entityAttribute;

    public Value(final Attribute<T> inputAttribute, final T entityValue, final boolean isNew, final Errors errors) {
        this(
            inputAttribute,
            inputAttribute.copy().setValue(entityValue),
            isNew,
            errors
        );
    }

    public Value(final Attribute<T> input, final Attribute<T> entityAttribute, final boolean isNew, final Errors errors) {
        super(input, isNew, errors);
        this.entityAttribute = entityAttribute;
    }


    @Override
    public Attribute<T> getAttribute() {
        if (super.getAttribute().isInitialized() || this.isNew()) {
            return super.getAttribute();
        }

        return this.entityAttribute;
    }

    public AttributeValue<T> onlyInput() {
        return new AttributeValue<>(super.getAttribute(), this.isNew(), this.getErrors());
    }

    public AttributeValue<T> onlyEntity() {
        return new AttributeValue<>(this.entityAttribute, this.isNew(), this.getErrors());
    }

    /**
     * Checks if the value contains a change (difference between entity and input value).
     *
     * @return {@code true}, if the input is a change due to the entity, {@code false} otherwise.
     */
    public boolean isChanged() {
        if (!this.onlyInput().getAttribute().isInitialized()) {
            return false;
        }

        Object entityValue = this.onlyEntity().getValue().orElse(null);
        Object inputValue = this.onlyInput().getValue().orElse(null);
        if (entityValue instanceof BigDecimal && inputValue instanceof BigDecimal) {
            return !compareEquals((BigDecimal) entityValue, (BigDecimal) inputValue);
        }

        if (!Objects.equals(this.onlyEntity().getAttribute().isInitialized(), this.onlyInput().getAttribute().isInitialized())) {
            return true;
        }

        return !Objects.equals(this.onlyEntity().getValue(), this.onlyInput().getValue());
    }

    public static <T> boolean compareEquals(BigDecimal value1, BigDecimal value2) {
        if (value1 == null && value2 == null) {
            return true;
        }

        if (value1 == null ||value2 == null) {
            return false;
        }

        return value1.compareTo(value2) == 0;
    }
}
