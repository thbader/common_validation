package sh.bader.common.validation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Errors {
    public List<Error> errors = new LinkedList<>();

    public Errors() {
    }

    Errors(final List<Error> errors) {
        this.errors.addAll(errors);
    }

    public List<Error> getErrors() {
        return Collections.unmodifiableList(this.errors);
    }

    public boolean hasErrors() {
        return !this.errors.isEmpty();
    }

    private Stream<Error> globalErrorStream() {
        return this.errors.stream().filter(error -> Error.Type.GLOBAL.equals(error.getType()));
    }

    @JsonIgnore
    public List<Error> getGlobalErrors() {
        return this.globalErrorStream().collect(Collectors.toUnmodifiableList());
    }

    public boolean hasGlobalErrors() {
        return this.globalErrorStream().findAny().isPresent();
    }

    private Stream<Error> attributeErrorStream() {
        return this.errors.stream().filter(error -> Error.Type.ATTRIBUTE.equals(error.getType()));
    }

    @JsonIgnore
    public List<Error> getAttributeErrors() {
        return this.attributeErrorStream().collect(Collectors.toUnmodifiableList());
    }

    public boolean hasAttributeErrors() {
        return this.attributeErrorStream().findAny().isPresent();
    }

    private Stream<Error> attributeErrorStream(final String path) {
        return this.attributeErrorStream().filter(error -> error.getPaths().contains(path));
    }

    public List<Error> getAttributeErrors(final String path) {
        return this.attributeErrorStream(path).collect(Collectors.toUnmodifiableList());
    }

    public boolean hasAttributeErrors(final String path) {
        return this.attributeErrorStream(path).findAny().isPresent();
    }

    public void reject(final Error error) {
        this.errors.add(error);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("errors", this.errors)
            .toString();
    }
}
