package sh.bader.common.validation.attribute.impl;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.AbstractAttributeValidator;
import sh.bader.common.validation.attribute.AttributeValidator;

import java.util.function.Function;
import java.util.function.Predicate;

import static sh.bader.common.validation.attribute.AttributeValidators.isInitialized;

public class UnifiedAttributeValidator<T> implements AttributeValidator<T> {

    private final Function<T, T> attributeFunction;

    private final AttributeValidator<T> validator;

    public UnifiedAttributeValidator(final Function<T, T> attributeFunction, AttributeValidator<T> validator) {
        this.attributeFunction = attributeFunction;
        this.validator = validator;
    }

    @Override
    public Result validate(Attribute<T> attribute) {
        Attribute<T> innerAttribute = attribute.copy();
        innerAttribute.getValue().ifPresent(value -> {
            T newValue = attributeFunction.apply(value);
            innerAttribute.setValue(newValue);
        });

        return validator.validate(innerAttribute);
    }
}
