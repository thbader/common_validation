package sh.bader.common.validation.attribute.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.value.AttributeValue;

public class ConditionalValidator<E, X> implements AttributeValidator<E> {

    private final List<AttributeValidator<E>> validators;

    @SafeVarargs
    public ConditionalValidator(final AttributeValue<X> value, final Predicate<AttributeValue<X>> requirement, final Function<X, AttributeValidator<E>>... validatorFunctions) {
        if (requirement.test(value)) {
            this.validators = Arrays.stream(validatorFunctions)
                .map(validator -> validator.apply(value.getValue().orElse(null)))
                .collect(Collectors.toUnmodifiableList());
        } else {
            this.validators = Collections.emptyList();
        }
    }

    @Override
    public Result validate(final Attribute<E> attribute) {
        for (final AttributeValidator<E> validator : this.validators) {
            final Result result = validator.validate(attribute);
            if (!result.isValid()) {
                return result;
            }
        }

        return Result.success();
    }

    public static <E> Predicate<AttributeValue<E>> isValid() {
        return new ValidPredicate<>();
    }

    public static <E> Predicate<AttributeValue<E>> isInitialized() {
        return new InitializedPredicate<>();
    }

    public static <E> Predicate<AttributeValue<E>> withValue(final Predicate<E> predicate) {
        return new ValuePredicate<>(predicate);
    }

    private static class ValidPredicate<E> implements Predicate<AttributeValue<E>> {
        @Override
        public boolean test(final AttributeValue<E> value) {
            return value.isValid();
        }
    }

    private static class InitializedPredicate<E> implements Predicate<AttributeValue<E>> {
        @Override
        public boolean test(final AttributeValue<E> value) {
            return value.getAttribute().isInitialized();
        }
    }

    private static class ValuePredicate<E> implements Predicate<AttributeValue<E>> {
        private final Predicate<E> predicate;

        public ValuePredicate(final Predicate<E> predicate) {
            this.predicate = predicate;
        }

        @Override
        public boolean test(final AttributeValue<E> value) {
            return predicate.test(value.getValue().orElse(null));
        }
    }
}
