package sh.bader.common.validation.attribute.impl.path;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isFile;

public final class FileNotEmptyValidator extends AbstractValueValidator<Path> {

    public static final String CODE = CODE_PREFIX + "path.fileNotEmpty";

    public FileNotEmptyValidator() {
        super(isFile());
    }

    @Override
    protected Error getError(final Attribute<Path> attribute) {
        return Error.attribute(attribute.getPath(), CODE);
    }

    @Override
    protected Predicate<Path> valuePredicate() {
        return path -> size(path) > 0;
    }

    private static Long size(final Path path) {
        try (final FileChannel channel = FileChannel.open(path)) {
            return channel.size();
        } catch (final IOException e) {
            return 0L;
        }
    }
}
