package sh.bader.common.validation.attribute;

import java.nio.file.Path;
import java.time.temporal.Temporal;
import java.util.function.Function;
import java.util.function.Predicate;

import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.impl.*;
import sh.bader.common.validation.attribute.impl.booleantype.BooleanTrueValidator;
import sh.bader.common.validation.attribute.impl.path.ExistsValidator;
import sh.bader.common.validation.attribute.impl.path.FileNotEmptyValidator;
import sh.bader.common.validation.attribute.impl.path.FileValidator;
import sh.bader.common.validation.attribute.impl.string.DigitValidator;
import sh.bader.common.validation.attribute.impl.string.LengthValidator;
import sh.bader.common.validation.attribute.impl.string.StartsWithValidator;
import sh.bader.common.validation.value.AttributeValue;

import static sh.bader.common.validation.attribute.impl.ConditionalValidator.isValid;
import static sh.bader.common.validation.attribute.impl.ConditionalValidator.withValue;

public final class AttributeValidators {

    private static final AttributeValidator<String> DIGITS_ONLY = new DigitValidator();

    private static final AttributeValidator<Boolean> BOOLEAN_TRUE = new BooleanTrueValidator();

    private AttributeValidators() {
    }

    public static <T> AttributeValidator<T> isInitialized() {
        return new InitializedValidator<>();
    }

    public static <T> AttributeValidator<T> isNotInitialized() {
        return new NotInitializedValidator<>();
    }

    public static <T> AttributeValidator<T> isNull() {
        return new NullValidator<>();
    }

    public static <T> AttributeValidator<T> isNotNull() {
        return new NotNullValidator<>();
    }

    /**
     * Get a validator to assert the max length of a string.
     *
     * @param max the max length.
     * @return the validator.
     */
    public static AttributeValidator<String> hasLength(final int max) {
        return new LengthValidator(max);
    }

    /**
     * Get a validator to assert the min and max length of a string.
     *
     * @param min the min length.
     * @param max the max length.
     * @return the validator.
     */
    public static AttributeValidator<String> hasLength(final int min, final int max) {
        return new LengthValidator(min, max);
    }

    /**
     * Get a validator to assert the value consists of digits only.
     *
     * @return the validator.
     */
    public static AttributeValidator<String> hasDigitsOnly() {
        return DIGITS_ONLY;
    }

    /**
     * Get a validator to assert the value is <code>true</code>.
     *
     * @return the validator.
     */
    public static AttributeValidator<Boolean> isBooleanTrue() {
        return BOOLEAN_TRUE;
    }

    /**
     * Get a validator to assert the number is greater than the limit.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isGreaterThan(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.GT, limit);
    }

    /**
     * Get a validator to assert the number is greater or equal than the limit.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isGreaterEqual(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.GE, limit);
    }

    /**
     * Get a validator to assert the number is less than the limit.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isLessThan(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.LT, limit);
    }

    /**
     * Get a validator to assert the number is less or equal than the limit.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isLessEqual(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.LE, limit);
    }

    /**
     * Get a validator to assert the number is equal to the value.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isEqual(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.EQ, limit);
    }

    /**
     * Get a validator to assert the number is not equal to the value.
     *
     * @param limit the limit.
     * @param <N>   the type of the number.
     * @return the validator.
     */
    public static <N extends Number> AttributeValidator<N> isNotEqual(final N limit) {
        return new NumberValidator<>(NumberValidator.Operator.NE, limit);
    }

    /**
     * Get a validator to assert the temporal is greater than the limit.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> isGreaterThan(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.GT, limit);
    }

    /**
     * Get a validator to assert the temporal is greater or equal than the limit.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> isGreaterEqual(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.GE, limit);
    }

    /**
     * Get a validator to assert the number is equal to the value.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> isEqual(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.EQ, limit);
    }

    /**
     * Get a validator to assert the number is not equal to the value.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> iNotsEqual(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.NE, limit);
    }

    /**
     * Get a validator to assert the temporal is less than the limit.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> isLessThan(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.LT, limit);
    }

    /**
     * Get a validator to assert the temporal is less or equal than the limit.
     *
     * @param limit the limit.
     * @param <T>   the type of the temporal.
     * @return the validator.
     */
    public static <T extends Temporal & Comparable<? extends Temporal>> AttributeValidator<T> isLessEqual(final T limit) {
        return new TemporalValidator<>(TemporalValidator.Operator.LE, limit);
    }

    @SafeVarargs
    public static <E, X> AttributeValidator<E> ifValue(final AttributeValue<X> value, final Predicate<X> predicate, final Function<X, AttributeValidator<E>>... validatorFunctions) {
        return new ConditionalValidator<>(value, ConditionalValidator.<X>isValid().and(withValue(predicate)), validatorFunctions);
    }

    @SafeVarargs
    public static <E, X> AttributeValidator<E> ifValid(final AttributeValue<X> value, final Function<X, AttributeValidator<E>>... validatorFunctions) {
        return new ConditionalValidator<>(value, isValid(), validatorFunctions);
    }

    @SafeVarargs
    public static <E, X> AttributeValidator<E> ifInitialized(final AttributeValue<X> value, final Function<X, AttributeValidator<E>>... validatorFunctions) {
        return new ConditionalValidator<>(value, ConditionalValidator.isInitialized(), validatorFunctions);
    }

    public static <T> AttributeValidator<T> toSuccess() {
        return value -> Result.success();
    }

    public static <T> AttributeValidator<T> toIgnored() {
        return value -> Result.ignored();
    }

    public static AttributeValidator<String> startsWith(final String pattern, final String... otherPatterns) {
        return new StartsWithValidator(pattern, otherPatterns);
    }

    public static AttributeValidator<Path> exists() {
        return new ExistsValidator();
    }

    public static AttributeValidator<Path> isFile() {
        return new FileValidator();
    }

    public static AttributeValidator<Path> isFileNotEmpty() {
        return new FileNotEmptyValidator();
    }

    public static <T> AttributeValidator<T> isEqual(T referenceValue) {
        return new EqualValidator<>(referenceValue);
    }

    public static AttributeValidator<String> isEqual(String referenceValue, String code) {
        return new EqualValidator(referenceValue, code);
    }

    public static <T> AttributeValidator<T> isNotEqual(T referenceValue) {
        return new NotEqualValidator<>(referenceValue);
    }

    public static AttributeValidator<String> isNotEqual(String referenceValue, String code) {
        return new NotEqualValidator(referenceValue, code);
    }

    public static <E> AttributeValidator<E> withUnifiedAttribute(final Function<E, E> attributeFunction, final AttributeValidator<E> validator) {
        return new UnifiedAttributeValidator<>(attributeFunction, validator);
    }
}
