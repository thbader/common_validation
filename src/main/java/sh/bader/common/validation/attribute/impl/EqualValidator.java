package sh.bader.common.validation.attribute.impl;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import java.util.Objects;
import java.util.function.Predicate;

public final class EqualValidator<T> extends AbstractValueValidator<T> {

    public static final String CODE = CODE_PREFIX + "isEqual";

    private final T referenceValue;

    private final String code;

    public EqualValidator(final T referenceValue) {
        this(referenceValue, CODE);
    }

    public EqualValidator(final T referenceValue, String code) {
        this.referenceValue = referenceValue;
        this.code = code;
    }

    @Override
    protected Error getError(final Attribute<T> attribute) {
        return Error.attribute(attribute.getPath(), code);
    }

    @Override
    protected Predicate<T> valuePredicate() {
        return value -> Objects.equals(referenceValue, value);
    }
}
