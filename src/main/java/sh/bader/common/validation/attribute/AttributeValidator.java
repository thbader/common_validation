package sh.bader.common.validation.attribute;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Result;

public interface AttributeValidator<T> {
    Result validate(Attribute<T> attribute);

    default AttributeValidator<T> or(final AttributeValidator<T> validator) {
        return (value) -> {
            final Result thisResult = this.validate(value);
            if (thisResult.isValid()) {
                return Result.success();
            }

            if (validator.validate(value).isValid()) {
                return Result.success();
            }

            return thisResult;

        };
    }

    default AttributeValidator<T> and(final AttributeValidator<T> validator) {
        return (value) -> {
            Result thisResult = this.validate(value);
            if (thisResult.isValid()) {
                thisResult = validator.validate(value);
                if (thisResult.isValid()) {
                    return Result.success();
                }
            }

            return thisResult;

        };
    }
}
