package sh.bader.common.validation.attribute.impl.string;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Argument;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

public final class StartsWithValidator extends AbstractValueValidator<String> {

    public static final String CODE = CODE_PREFIX + "string.startsWith";

    private final List<String> patterns;

    public StartsWithValidator(final String pattern, final String... otherPatterns) {
        this(Arrays.asList(ArrayUtils.add(otherPatterns, pattern)));
    }

    public StartsWithValidator(final List<String> patterns) {
        super(isNotNull());

        if (patterns == null) {
            this.patterns = Collections.emptyList();
        } else {
            this.patterns = patterns.stream()
                .filter(StringUtils::isNotBlank)
                .map(StringUtils::trim)
                .collect(Collectors.toUnmodifiableList());
        }

        if (this.patterns.isEmpty()) {
            throw new IllegalArgumentException("missing value: patterns");
        }
    }

    @Override
    protected Error getError(final Attribute<String> attribute) {
        return Error.attribute(
            attribute.getPath(),
            CODE,
            new Argument("pattern", this.patterns)
        );
    }

    @Override
    protected Predicate<String> valuePredicate() {
        return value -> patterns.stream().anyMatch(value::startsWith);
    }
}
