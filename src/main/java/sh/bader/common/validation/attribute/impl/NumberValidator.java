package sh.bader.common.validation.attribute.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Argument;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

public final class NumberValidator<T extends Number> extends AbstractValueValidator<T> {

    public enum Operator {
        LT {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) < 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.lessThan";
            }
        },
        LE {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) <= 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.lessEquals";
            }
        },
        EQ {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) == 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.equals";
            }
        },
        NE {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) != 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.notEquals";
            }
        },
        GE {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) >= 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.greaterEquals";
            }
        },
        GT {
            @Override
            boolean isValid(final BigDecimal value, final BigDecimal limit) {
                return value.compareTo(limit) > 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "number.greaterThan";
            }
        };

        /**
         * Check if the comparison of the value with the comparedTo is valid.
         *
         * @param value the value.
         * @param limit the limit.
         * @return {@code true}, if the comparison is valid, {@code false} otherwise.
         */
        abstract boolean isValid(BigDecimal value, BigDecimal limit);

        abstract String getCode();
    }

    private final Operator operator;

    private final T limit;

    public NumberValidator(final Operator operator, final T limit) {
        super(isNotNull());
        if (operator == null) {
            throw new IllegalArgumentException("missing value: operator");
        }
        if (limit == null) {
            throw new IllegalArgumentException("missing value: limit");
        }

        this.operator = operator;
        this.limit = limit;
    }

    @Override
    protected Error getError(final Attribute<T> attribute) {
        return Error.attribute(attribute.getPath(), this.operator.getCode(), new Argument("limit", Collections.singletonList(this.limit)));
    }

    @Override
    protected Predicate<T> valuePredicate() {
        return value -> operator.isValid(toBigDecimal(value), toBigDecimal(limit));
    }

    private static BigDecimal toBigDecimal(final Number number) {
        BigDecimal result = null;

        if (number != null) {
            if (number instanceof BigDecimal) {
                result = (BigDecimal) number;
            } else if (number instanceof BigInteger) {
                result = new BigDecimal((BigInteger) number);
            } else if (number instanceof Integer || number instanceof Long || number instanceof AtomicInteger
                || number instanceof AtomicLong
                || number instanceof Byte || number instanceof Short) {
                result = BigDecimal.valueOf(number.longValue());
            } else if (number instanceof Double || number instanceof Float) {
                result = BigDecimal.valueOf(number.doubleValue());
            }
        }

        return result;
    }
}
