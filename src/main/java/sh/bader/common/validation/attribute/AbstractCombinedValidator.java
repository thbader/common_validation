package sh.bader.common.validation.attribute;

import java.util.Arrays;
import java.util.List;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Result;

public abstract class AbstractCombinedValidator<T> implements AttributeValidator<T> {
    private final List<AttributeValidator<T>> validators;

    @SafeVarargs
    public AbstractCombinedValidator(AttributeValidator<T>... validators) {
        this.validators = Arrays.asList(validators);
    }

    @Override
    public Result validate(Attribute<T> attribute) {
        for (final AttributeValidator<T> validator : validators) {
            Result result = validator.validate(attribute);
            if (!result.isValid()) {
                return result;
            }
        }

        return Result.success();
    }
}
