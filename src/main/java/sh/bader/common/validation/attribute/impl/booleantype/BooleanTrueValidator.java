package sh.bader.common.validation.attribute.impl.booleantype;

import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

public final class BooleanTrueValidator extends AbstractValueValidator<Boolean> {
    public static final String CODE = CODE_PREFIX + "boolean.notTrue";

    public BooleanTrueValidator() {
        super(isNotNull());
    }

    @Override
    protected Error getError(Attribute<Boolean> attribute) {
        return Error.attribute(attribute.getPath(), CODE);
    }

    @Override
    protected Predicate<Boolean> valuePredicate() {
        return Boolean.TRUE::equals;
    }
}
