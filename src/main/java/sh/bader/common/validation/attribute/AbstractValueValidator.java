package sh.bader.common.validation.attribute;

import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;

public abstract class AbstractValueValidator<T> extends AbstractAttributeValidator<T> {
    @SafeVarargs
    public AbstractValueValidator(final AttributeValidator<T>... requirements) {
        super(requirements);
    }

    @Override
    protected final Predicate<Attribute<T>> attributePredicate() {
        return new AttributePredicate<>(this.valuePredicate());
    }

    protected abstract Predicate<T> valuePredicate();

    private static class AttributePredicate<T> implements Predicate<Attribute<T>> {
        private final Predicate<T> predicate;

        public AttributePredicate(final Predicate<T> predicate) {
            this.predicate = predicate;
        }

        @Override
        public boolean test(final Attribute<T> attribute) {
            return this.predicate.test(attribute.getValue().orElse(null));
        }
    }
}
