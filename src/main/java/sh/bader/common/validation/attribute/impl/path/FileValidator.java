package sh.bader.common.validation.attribute.impl.path;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.exists;

public final class FileValidator extends AbstractValueValidator<Path> {

    public static final String CODE = CODE_PREFIX + "path.isFile";

    public FileValidator() {
        super(exists());
    }

    @Override
    protected Error getError(final Attribute<Path> attribute) {
        return Error.attribute(attribute.getPath(), CODE);
    }

    @Override
    protected Predicate<Path> valuePredicate() {
        return Files::isRegularFile;
    }
}
