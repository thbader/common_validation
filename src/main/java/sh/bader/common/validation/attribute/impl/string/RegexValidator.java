package sh.bader.common.validation.attribute.impl.string;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

abstract class RegexValidator extends AbstractValueValidator<String> {
    private final Pattern pattern;
    private final String code;

    public RegexValidator(final Pattern pattern, final String code) {
        super(isNotNull());
        this.pattern = pattern;
        this.code = code;
    }

    @Override
    protected final Error getError(final Attribute<String> attribute) {
        return Error.attribute(attribute.getPath(), code);
    }

    @Override
    protected final Predicate<String> valuePredicate() {
        return value -> pattern.matcher(value).matches();
    }
}
