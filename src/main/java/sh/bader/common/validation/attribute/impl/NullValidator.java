package sh.bader.common.validation.attribute.impl;

import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractAttributeValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isInitialized;

public final class NullValidator<T> extends AbstractAttributeValidator<T> {
    public static final String CODE = CODE_PREFIX + "isNull";

    public NullValidator() {
        super(isInitialized());
    }

    @Override
    protected Error getError(final Attribute<T> attribute) {
        return Error.attribute(attribute.getPath(), CODE);
    }

    @Override
    protected Predicate<Attribute<T>> attributePredicate() {
        return attribute -> attribute.getValue().isEmpty();
    }
}
