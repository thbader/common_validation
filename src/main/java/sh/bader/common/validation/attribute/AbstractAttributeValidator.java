package sh.bader.common.validation.attribute;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;

public abstract class AbstractAttributeValidator<T> implements AttributeValidator<T> {

    private final Logger log = LogManager.getLogger(this.getClass());

    protected final static String CODE_PREFIX = "validation.error.";

    private final List<AttributeValidator<T>> requirements;

    @SafeVarargs
    public AbstractAttributeValidator(final AttributeValidator<T>... requirements) {
        this.requirements = Stream.of(requirements)
            .filter(Objects::nonNull)
            .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public final Result validate(final Attribute<T> attribute) {
        final Result result = this.validateRequirements(attribute);

        if (Result.Type.SUCCESS.equals(result.getType())) {
            if (this.attributePredicate().test(attribute)) {
                this.log.trace("[{}] validation success for {}", attribute.getPath(), attribute);
                return Result.success();
            } else {
                final Error error = this.getError(attribute);

                this.log.trace("[{}] validation failed for {}: {}", attribute.getPath(), attribute, error);
                return Result.error(error);
            }
        }

        this.log.trace("[{}] validation ignored for {}", attribute.getPath(), attribute);
        return Result.ignored();
    }

    protected abstract Error getError(Attribute<T> attribute);

    protected abstract Predicate<Attribute<T>> attributePredicate();

    private Result validateRequirements(final Attribute<T> attribute) {
        for (final AttributeValidator<T> requirement : this.requirements) {
            final Result actual = requirement.validate(attribute);
            if (!Result.Type.SUCCESS.equals(actual.getType())) {
                return actual;
            }
        }

        return Result.success();
    }
}
