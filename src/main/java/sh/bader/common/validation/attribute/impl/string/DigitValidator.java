package sh.bader.common.validation.attribute.impl.string;

import java.util.regex.Pattern;

public final class DigitValidator extends RegexValidator {
    public static final String CODE = CODE_PREFIX + "string.digitsOnly";

    private static final Pattern PATTERN = Pattern.compile("\\d+");

    public DigitValidator() {
        super(PATTERN, CODE);
    }
}
