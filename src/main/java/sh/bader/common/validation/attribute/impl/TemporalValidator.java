package sh.bader.common.validation.attribute.impl;

import java.time.temporal.Temporal;
import java.util.Collections;
import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Argument;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

public class TemporalValidator<T extends Temporal & Comparable<? extends Temporal>> extends AbstractValueValidator<T> {

    public enum Operator {
        LT {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) < 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.lessThan";
            }
        },
        LE {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) <= 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.lessEquals";
            }
        },
        EQ {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) == 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.equals";
            }
        },
        NE {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) != 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.notEquals";
            }
        },
        GE {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) >= 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.greaterEquals";
            }
        },
        GT {
            @Override
            boolean isValid(final Comparable<Temporal> value, final Temporal limit) {
                return value.compareTo(limit) > 0;
            }

            @Override
            String getCode() {
                return CODE_PREFIX + "temporal.greaterThan";
            }
        };

        /**
         * Check if the comparison of the value with the comparedTo is valid.
         *
         * @param value the value.
         * @param limit the limit.
         * @return {@code true}, if the comparison is valid, {@code false} otherwise.
         */
        abstract boolean isValid(Comparable<Temporal> value, Temporal limit);

        abstract String getCode();
    }

    private final Operator operator;

    private final T limit;

    public TemporalValidator(final Operator operator, final T limit) {
        super(isNotNull());
        this.operator = operator;
        this.limit = limit;
    }

    @Override
    protected Error getError(final Attribute<T> attribute) {
        return Error.attribute(attribute.getPath(), this.operator.getCode(), new Argument("limit", Collections.singletonList(this.limit)));
    }

    @Override
    protected Predicate<T> valuePredicate() {
        return value -> operator.isValid((Comparable<Temporal>) value, limit);
    }
}
