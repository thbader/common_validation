package sh.bader.common.validation.attribute.impl.string;

import java.util.Collections;
import java.util.function.Predicate;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Argument;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.attribute.AbstractValueValidator;

import static sh.bader.common.validation.attribute.AttributeValidators.isNotNull;

public final class LengthValidator extends AbstractValueValidator<String> {

    public static final String CODE = CODE_PREFIX + "string.length";

    private final int min;

    private final int max;

    public LengthValidator(final int max) {
        this(0, max);
    }

    public LengthValidator(final int min, final int max) {
        super(isNotNull());

        if (max <= 0) {
            throw new IllegalArgumentException("invalid value: max=" + max);
        }

        if (min < 0) {
            throw new IllegalArgumentException("invalid value min=" + min);
        } else if (min > max) {
            throw new IllegalArgumentException("min is greater then max: min=" + min + " / max=" + max);
        }

        this.min = min;
        this.max = max;
    }

    @Override
    protected Error getError(final Attribute<String> attribute) {
        return Error.attribute(
            attribute.getPath(),
            CODE,
            new Argument("min", Collections.singletonList(this.min)),
            new Argument("max", Collections.singletonList(this.max))
        );
    }

    @Override
    protected Predicate<String> valuePredicate() {
        return value -> this.min <= value.length() && value.length() <= this.max;
    }
}
