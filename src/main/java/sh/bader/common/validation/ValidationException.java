package sh.bader.common.validation;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class ValidationException extends RuntimeException {
    private final Errors errors;

    public ValidationException(final Errors errors) {
        this.errors = errors == null ? new Errors() : errors;
    }

    public Errors getErrors() {
        return errors;
    }

    @Override
    public String getMessage() {
        return "errors=" + errors.getErrors();
    }

    public static <E> E onErrorsWithReturn(final Function<Errors, E> function) {
        final Errors errors = new Errors();

        final E result = function.apply(errors);

        if (errors.hasErrors()) {
            throw new ValidationException(errors);
        }

        return result;
    }

    public static <E> E onErrorsWithOptionalReturn(final Function<Errors, Optional<E>> function) {
        final Errors errors = new Errors();

        final Optional<E> result = function.apply(errors);

        if (errors.hasErrors()) {
            throw new ValidationException(errors);
        }

        return result.orElseThrow(() -> new IllegalArgumentException("function returns nothing"));
    }

    public static void onErrors(final Consumer<Errors> consumer) {
        final Errors errors = new Errors();

        consumer.accept(errors);

        if (errors.hasErrors()) {
            throw new ValidationException(errors);
        }
    }
}
