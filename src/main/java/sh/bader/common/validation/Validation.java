package sh.bader.common.validation;

import java.util.ArrayList;
import java.util.List;

import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.value.Value;

public class Validation {
    private final boolean isNew;

    private final Errors errors;

    private final List<Value<?>> values = new ArrayList<>();

    public Validation(final boolean isNew, final Errors errors) {
        this.isNew = isNew;
        this.errors = errors;
    }

    public Errors getErrors() {
        return this.errors;
    }

    public List<Value<?>> getValues() {
        return values;
    }

    public <T> Value<T> forAttribute(final Attribute<T> attribute, final T entityValue) {
        Value<T> value = new Value<>(attribute, entityValue, this.isNew, this.errors);
        values.add(value);

        return value;
    }

    public <T> Value<T> forAttribute(final Attribute<T> attribute) {
        Value<T> value = new Value<>(attribute, attribute.uninitializedCopy(), this.isNew, this.errors);
        values.add(value);

        return value;
    }

    /**
     * Checks if there is any value would result in a change.
     *
     * @return {@code true}, if any value would result in a change, {@code false otherwise}.
     */
    public boolean isChanged() {
        if (isNew) {
            return true;
        }

        return values.stream().anyMatch(Value::isChanged);
    }
}
