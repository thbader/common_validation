package sh.bader.common.validation;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Error {
    public enum Type {
        GLOBAL,
        ATTRIBUTE
    }

    private final Type type;

    private final List<String> paths;

    private final String code;

    private final List<Argument> arguments = new LinkedList<>();

    private Error(final Type type, final List<String> paths, final String code, final List<Argument> arguments) {
        this.type = type;
        this.paths = Collections.unmodifiableList(paths);
        this.code = code;
        this.arguments.addAll(arguments);
    }

    public static Error global(final String code, final Argument... arguments) {
        return new Error(Type.GLOBAL, Collections.emptyList(), code, Arrays.asList(arguments));
    }

    public static Error attribute(final String path, final String code, final Argument... arguments) {
        return attribute(path, code, Arrays.asList(arguments));
    }

    public static Error attribute(final String path, final String code, final List<Argument> arguments) {
        return attribute(Collections.singletonList(path), code, arguments);
    }

    public static Error attribute(final List<String> paths, final String code, final Argument... arguments) {
        return attribute(paths, code, Arrays.asList(arguments));
    }

    public static Error attribute(final List<String> paths, final String code, final List<Argument> arguments) {
        return new Error(Type.ATTRIBUTE, paths, code, arguments);
    }

    public Type getType() {
        return this.type;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<String> getPaths() {
        return this.paths;
    }

    public String getCode() {
        return this.code;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<Argument> getArguments() {
        return Collections.unmodifiableList(this.arguments);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("type", this.type)
            .append("paths", this.paths)
            .append("code", this.code)
            .append("arguments", this.arguments)
            .toString();
    }
}
