package sh.bader.common.validation.attribute.impl.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.impl.path.FileValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class FileValidatorTest {
    private final static FileValidator VALIDATOR = new FileValidator();

    private Attribute<Path> attribute;

    private Path file;

    private Path directory;

    @BeforeEach
    public void before() throws IOException {
        this.attribute = Attribute.uninitialized("file");
        this.file = Files.createTempFile("abc", "xyz");
        this.directory = Files.createTempDirectory("123");

        this.file.toFile().deleteOnExit();
        this.directory.toFile().deleteOnExit();
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        this.attribute.setValue((Path) null);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateNotExisting() throws IOException {
        // given
        Files.delete(directory);
        this.attribute.setValue(directory);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithDirectory() {
        // given
        this.attribute.setValue(directory);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(FileValidator.CODE))
            )))
        ));
    }

    @Test
    public void validateInitializedWithFile() {
        // given
        this.attribute.setValue(file);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }
}
