package sh.bader.common.validation.attribute.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.AttributeValidator;
import sh.bader.common.validation.attribute.impl.InitializedValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class InitializedValidatorTest {
    private final AttributeValidator<String> VALIDATOR = new InitializedValidator<>();

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        this.attribute = Attribute.uninitialized("name");
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = this.VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(InitializedValidator.CODE))
            )))
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        this.attribute.setValue((String) null);

        // when
        final Result result = this.VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithValue() {
        // given
        this.attribute.setValue("123");

        // when
        final Result result = this.VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }
}
