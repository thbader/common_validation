package sh.bader.common.validation.attribute.impl.string;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.impl.string.LengthValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LengthValidatorTest {
    private final static int MIN = 2;
    private final static int MAX = 3;
    private final static LengthValidator VALIDATOR = new LengthValidator(MIN, MAX);

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        this.attribute = Attribute.uninitialized("name");
    }

    @Test
    public void initMaxZero() {
        assertThrows(IllegalArgumentException.class, () -> new LengthValidator(0));
    }

    @Test
    public void initMinLowerThanZero() {
        assertThrows(IllegalArgumentException.class, () -> new LengthValidator(-1, 1));
    }

    @Test
    public void initMaxLowerMin() {
        assertThrows(IllegalArgumentException.class, () -> new LengthValidator(2, 1));
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        this.attribute.setValue((String) null);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithTooShort() {
        // given
        this.attribute.setValue("1");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(LengthValidator.CODE)),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("min")),
                    hasProperty("values", contains(MIN))
                ), allOf(
                    hasProperty("key", equalTo("max")),
                    hasProperty("values", contains(MAX))
                )))
            )))
        ));
    }

    @Test
    public void validateInitializedWithValidLength() {
        // given
        this.attribute.setValue("12");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithTooLong() {
        // given
        this.attribute.setValue("1234");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(LengthValidator.CODE)),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("min")),
                    hasProperty("values", contains(MIN))
                ), allOf(
                    hasProperty("key", equalTo("max")),
                    hasProperty("values", contains(MAX))
                )))
            )))
        ));
    }
}
