package sh.bader.common.validation.attribute;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.AttributeValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class AttributeValidatorTest {
    public static final String ERROR_CODE = "anError";

    private final AttributeValidator<String> VALIDATOR_SUCCESS = attribute -> Result.success();
    private final AttributeValidator<String> VALIDATOR_ERROR = attribute -> Result.error(Error.attribute(attribute.getPath(), ERROR_CODE));

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        this.attribute = Attribute.uninitialized("name");
    }

    @Test
    public void orWithAllSuccess() {
        // given

        // when
        final Result result = this.VALIDATOR_SUCCESS.or(VALIDATOR_SUCCESS).validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void orWithFirstSuccess() {
        // given

        // when
        final Result result = this.VALIDATOR_SUCCESS.or(VALIDATOR_ERROR).validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void orWithLastSuccess() {
        // given

        // when
        final Result result = this.VALIDATOR_ERROR.or(VALIDATOR_SUCCESS).validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void orWithAlLError() {
        // given

        // when
        final Result result = this.VALIDATOR_ERROR.or(VALIDATOR_ERROR).validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(ERROR_CODE))
            )))
        ));
    }
}
