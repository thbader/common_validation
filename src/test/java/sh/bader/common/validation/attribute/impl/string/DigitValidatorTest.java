package sh.bader.common.validation.attribute.impl.string;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.impl.string.DigitValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

public class DigitValidatorTest {
    private final static DigitValidator VALIDATOR = new DigitValidator();

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        attribute = Attribute.uninitialized("name");
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = VALIDATOR.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        attribute.setValue((String) null);

        // when
        final Result result = VALIDATOR.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithDigitsOnly() {
        // given
        attribute.setValue("123");

        // when
        final Result result = VALIDATOR.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithCharactersOnly() {
        // given
        attribute.setValue("abc");

        // when
        final Result result = VALIDATOR.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(DigitValidator.CODE))
            )))
        ));
    }
}
