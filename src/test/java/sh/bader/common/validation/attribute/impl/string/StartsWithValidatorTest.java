package sh.bader.common.validation.attribute.impl.string;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StartsWithValidatorTest {
    private final static String PATTERN_1 = "http://";
    private final static String PATTERN_2 = "https://";
    private final static StartsWithValidator VALIDATOR = new StartsWithValidator(PATTERN_1, PATTERN_2);

    private Attribute<String> attribute;

    @BeforeEach
    public void before() {
        this.attribute = Attribute.uninitialized("name");
    }

    @Test
    public void initWithoutPattern() {
        assertThrows(IllegalArgumentException.class, () -> new StartsWithValidator(" "));
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        this.attribute.setValue((String) null);

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        //then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithInvalid() {
        // given
        this.attribute.setValue("http:/website.de");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(StartsWithValidator.CODE)),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("pattern")),
                    hasProperty("values", containsInAnyOrder(PATTERN_1, PATTERN_2))
                )))
            )))
        ));
    }

    @Test
    public void validateInitializedWithValidValue1() {
        // given
        this.attribute.setValue("http://www.sbader.de");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithValidValue2() {
        // given
        this.attribute.setValue("https://www.sbader.de");

        // when
        final Result result = VALIDATOR.validate(this.attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }
}
