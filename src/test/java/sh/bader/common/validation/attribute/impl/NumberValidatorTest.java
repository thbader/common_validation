package sh.bader.common.validation.attribute.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sh.bader.common.attribute.Attribute;
import sh.bader.common.validation.Error;
import sh.bader.common.validation.Result;
import sh.bader.common.validation.attribute.impl.NumberValidator;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isEmpty;
import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresentAnd;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static sh.bader.common.validation.attribute.impl.NumberValidator.Operator;

public class NumberValidatorTest {
    private static final int LOWER = -1;
    private static final int EQUAL = 0;
    private static final int GREATER = 1;

    private static final NumberValidator<Integer> VALIDATOR_GT = new NumberValidator<>(Operator.GT, EQUAL);
    private static final NumberValidator<Integer> VALIDATOR_GE = new NumberValidator<>(Operator.GE, EQUAL);
    private static final NumberValidator<Integer> VALIDATOR_EQ = new NumberValidator<>(Operator.EQ, EQUAL);
    private static final NumberValidator<Integer> VALIDATOR_LE = new NumberValidator<>(Operator.LE, EQUAL);
    private static final NumberValidator<Integer> VALIDATOR_LT = new NumberValidator<>(Operator.LT, EQUAL);

    private Attribute<Integer> attribute;

    @BeforeEach
    public void before() {
        attribute = Attribute.uninitialized("name");
    }

    @Test
    public void validateUninitialized() {
        // given

        // when
        final Result result = VALIDATOR_EQ.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateInitializedWithNull() {
        // given
        attribute.setValue((Integer) null);

        // when
        final Result result = VALIDATOR_EQ.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.IGNORED)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateGtInitializedWithLower() {
        // given
        attribute.setValue(LOWER);

        // when
        final Result result = VALIDATOR_GT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.GT.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateGtInitializedWithEqual() {
        // given
        attribute.setValue(EQUAL);

        // when
        final Result result = VALIDATOR_GT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.GT.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateGtInitializedWithGreater() {
        // given
        attribute.setValue(GREATER);

        // when
        final Result result = VALIDATOR_GT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateGeInitializedWithLower() {
        // given
        attribute.setValue(LOWER);

        // when
        final Result result = VALIDATOR_GE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.GE.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateGeInitializedWithEqual() {
        // given
        attribute.setValue(EQUAL);

        // when
        final Result result = VALIDATOR_GE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateGeInitializedWithGreater() {
        // given
        attribute.setValue(GREATER);

        // when
        final Result result = VALIDATOR_GE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateEqInitializedWithLower() {
        // given
        attribute.setValue(LOWER);

        // when
        final Result result = VALIDATOR_EQ.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.EQ.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateEqInitializedWithEqual() {
        // given
        attribute.setValue(EQUAL);

        // when
        final Result result = VALIDATOR_EQ.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateEqInitializedWithGreater() {
        // given
        attribute.setValue(GREATER);

        // when
        final Result result = VALIDATOR_EQ.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.EQ.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateLeInitializedWithLower() {
        // given
        attribute.setValue(LOWER);

        // when
        final Result result = VALIDATOR_LE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateLEInitializedWithEqual() {
        // given
        attribute.setValue(EQUAL);

        // when
        final Result result = VALIDATOR_LE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateLeInitializedWithGreater() {
        // given
        attribute.setValue(GREATER);

        // when
        final Result result = VALIDATOR_LE.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.LE.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateLtInitializedWithLower() {
        // given
        attribute.setValue(LOWER);

        // when
        final Result result = VALIDATOR_LT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.SUCCESS)),
            hasProperty("error", isEmpty())
        ));
    }

    @Test
    public void validateLtInitializedWithEqual() {
        // given
        attribute.setValue(EQUAL);

        // when
        final Result result = VALIDATOR_LT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.LT.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }

    @Test
    public void validateLtInitializedWithGreater() {
        // given
        attribute.setValue(GREATER);

        // when
        final Result result = VALIDATOR_LT.validate(attribute);

        // then
        assertThat(result, allOf(
            hasProperty("type", equalTo(Result.Type.ERROR)),
            hasProperty("error", isPresentAnd(allOf(
                hasProperty("type", equalTo(Error.Type.ATTRIBUTE)),
                hasProperty("paths", contains(this.attribute.getPath())),
                hasProperty("code", equalTo(Operator.LT.getCode())),
                hasProperty("arguments", contains(allOf(
                    hasProperty("key", equalTo("limit")),
                    hasProperty("values", contains(EQUAL))
                )))
            )))
        ));
    }
}
